// TOGGLE FULLSCREEN FUNCTIONALITY
const togglefullon = document.querySelector('.toggle-on');
const togglefulloff = document.querySelector('.toggle-off');

document.addEventListener('fullscreenchange', ev => {
    ev.preventDefault();
    if (!document.fullscreenElement) {
        togglefullon.setAttribute('class', 'toggle-on shown')
        togglefulloff.setAttribute('class', 'toggle-off hidden')
    } else if (document.fullscreenElement) {
        togglefullon.setAttribute('class', 'toggle-on hidden');
        togglefulloff.setAttribute('class', 'toggle-off shown')
    }
})

togglefullon.addEventListener('click', ev => {
    ev.preventDefault();
    document.querySelector('body').requestFullscreen();
});

togglefulloff.addEventListener('click', ev => {
    ev.preventDefault();
    document.exitFullscreen();
});

// TOGGLE KICK MENU FUNCTIONALITY

const menubtn = document.querySelector('.menu');
const kickmenu = document.querySelector('.kick');

const showmenu = ()=>{
    if (kickmenu.getAttribute('class').split(' ')[1] == 'hidden') {
        kickmenu.setAttribute('class', 'kick shown');
    } else if (kickmenu.getAttribute('class').split(' ')[1] == 'shown') {
        kickmenu.setAttribute('class', 'kick hidden');
    }
}

menubtn.addEventListener('click', ev => {
    ev.preventDefault();
    showmenu();
});

// START APPS EXPERIMENTAL FUNCTIONALITY

const apps = document.querySelectorAll('.app');
const dolphin = document.querySelector('#dolphin');
const discover = document.querySelector('#discover');

const openWindow = (app) => {
    app.setAttribute('class', 'app shown');
};

const closeWindow = (app) => {
    // Close all open windows
    if (app.length) {
        for (ap of app) {
            ap.setAttribute('class', 'app hidden');
        };
    } else { // Close requested app window
        app.setAttribute('class', 'app hidden');
    }
};

/*
open dolphin:

openWindow(dolphin);

closing dolphin:

closeWindow(dolphin);

close all windows:

closeWindow(apps);
*/

// RECTANGLE
// GET MOUSE POSITION

const wall = document.querySelector('.wallpaper');
const rectangle = document.querySelector('.selecrect');
const rectsvg = rectangle.querySelector('rect');
let inMouseX
let inMouseY
let finMouseX
let finMouseY

const MouseDrag = event => {
    event.preventDefault();
    finMouseX = event.x;
    finMouseY = event.y;
    console.log(`X: ${finMouseX}`)
    console.log(`Y: ${finMouseY}`)
    if (finMouseX < 0) {
        finMouseX = 0;
    }
    if(finMouseY < 0) {
        finMouseY = 0;
    }
    if (finMouseX > wall.clientWidth) {
        finMouseX = wall.clientWidth;
    }
    if (finMouseY > wall.clientHeight) {
        finMouseY = wall.clientHeight;
    }
    if (finMouseX-inMouseX < 0) {
        rectsvg.setAttribute('x', finMouseX);
        rectsvg.setAttribute('width', inMouseX-finMouseX)
    }
    if (finMouseY-inMouseY < 0) {
        rectsvg.setAttribute('y', finMouseY);
        rectsvg.setAttribute('height', inMouseY-finMouseY);
    }
    if (finMouseX-inMouseX >= 0) {
        rectsvg.setAttribute('x', inMouseX);
        rectsvg.setAttribute('width', finMouseX-inMouseX);
    }
    if (finMouseY-inMouseY >= 0) {
        rectsvg.setAttribute('y', inMouseY);
        rectsvg.setAttribute('height', finMouseY-inMouseY);
    }
};

const MouseUp = event => {
    event.preventDefault();
    rectangle.setAttribute('class', 'selecrect hidden');
    rectsvg.setAttribute('x', "0");
    rectsvg.setAttribute('y', "0");
    rectsvg.setAttribute('width', "0");
    rectsvg.setAttribute('height', "0");
    document.removeEventListener('mousemove', MouseDrag);
    document.removeEventListener('mouseup', MouseUp);
}


document.addEventListener("mousedown", (event) => {
    event.preventDefault();
    if (event.button == 2) {
        if (event.buttons == 3) {
            MouseUp(event);
            return
        }
        console.log('right click')
        return
    }
    inMouseX = event.x;
    inMouseY = event.y;
    rectsvg.setAttribute('x', inMouseX);
    rectsvg.setAttribute('y', inMouseY);
    rectangle.setAttribute('class', 'selecrect shown')
    // While mouse is moving
    document.addEventListener("mousemove", MouseDrag);
    document.addEventListener("mouseup", MouseUp);
});
